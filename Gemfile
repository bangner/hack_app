source 'https://rubygems.org'
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.0'

# Use sqlite3 as the database for Active Record
# gem 'sqlite3'

gem 'rails_12factor', group: :production

# Use PostgresSQL for DB
gem 'pg'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.0'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

# Makes console database output beautiful
gem 'hirb'

# Adds haml for awesome html and ruby
gem 'haml'

# Easily install backbone.js to a rails app
gem "rails-backbone"

group :development do
  gem 'pry'
end

# A css framework for use with scss see compass-style.org for details
# gem 'compass-rails'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

# Adds pretty interface and explanations to the error page
# Detailed errors
gem 'better_errors'

# Advanced feature for better errors
gem 'binding_of_caller'

# Full text search enabled with postgres
gem 'pg_search'

# Allows us to upload photos
gem 'carrierwave'

# Scales down images to the right size for each page
gem 'rmagick'

# Store environmental variables
gem 'figaro'

# Allows us to use fog for storage
gem 'fog'

# For disqus comments
gem 'disqus'

# allows us to use font-awesome icons
gem 'font-awesome-rails'
# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]
  