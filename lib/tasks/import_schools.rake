require 'csv'

# desc "Imports a CSV file into an ActiveRecord Table"

# csv_text = File.read("public/schoolinfo.csv")
task :import => :environment do 
  CSV.foreach('public/schoolinfo3.csv', :encoding => 'windows-1251:utf-8', :headers => true) do |row|
    School.create!(row.to_hash)
end
end

