class School < ActiveRecord::Base
  has_many :employees
  has_many :locations
  has_many :cities, through: :locations
  has_many :courses, through: :locations

  mount_uploader :attachment, ImageUploader

  include PgSearch
  pg_search_scope :search, against: [:name, :city, :stack, :price],
    using: {tsearch: {dictionary: "english"}}

  def self.text_search(query)
    if query.present?
      search(query)
    else
      School.all
    end
  end

  def self.filter(filter_params = nil)

    if filter_params

      schools = School.all

      puts schools

      selection = filter_params[:cities].values

      locations = selection.inject(School) { |schools, city| schools.where('city IN (?)', selection) }

      if locations.empty?
        locations = schools
      end

      tech = selection.inject(School) { |schools, city| schools.where('stack IN (?)', selection) }

      if tech.empty?
        tech = schools
      end

      results = []

      schools.each do |school|
        if tech.include?(school) && locations.include?(school)
          results << school
        end
      end

      results

    else
      School.all
    end
  end
end
