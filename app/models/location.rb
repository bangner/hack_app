class Location < ActiveRecord::Base
  belongs_to :city
  belongs_to :school
  has_many :courses
  has_one :address
end
