json.array!(@schools) do |school|
  json.extract! school, :name, :zip, :description, :price, :duration, :stack, :logo, :video, :image, :website, :email, :phone, :twitter, :facebook, :github
  json.url school_url(school, format: :json)
end
