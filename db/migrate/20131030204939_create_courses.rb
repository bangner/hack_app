class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.string :name
      t.text :description
      t.string :duration
      t.string :focus
      t.string :stack
      t.string :email
      t.string :video
      t.string :price
      t.references :location

      t.timestamps
    end
  end
end
