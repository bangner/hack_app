class CreateSchools < ActiveRecord::Migration
  def change
    create_table :schools do |t|
      t.string :name
      t.text :description
      t.string :website
      t.string :email
      t.string :twitter
      t.string :facebook
      t.text :logo
      t.string :video

      t.timestamps
    end
  end
end
