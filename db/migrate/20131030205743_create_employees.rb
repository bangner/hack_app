class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :name
      t.text :bio
      t.text :photo
      t.string :position
      t.references :school, index: true

      t.timestamps
    end
  end
end
