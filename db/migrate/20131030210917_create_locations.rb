class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.references :city, index: true
      t.references :school, index: true

      t.timestamps
    end
  end
end
